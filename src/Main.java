public class Main {
    public static void main(String[] args) {
        VicePresident vp = new VicePresident();
        System.out.println("===========VICE==========");
        vp.masukKerja();
        vp.beriNama("Agus");
        System.out.println("Nama : "+ vp.getNama());
        vp.setJumlahBranch(5);
        System.out.println("Jumlah Perusahaan : "+vp.getJumlahBranch());

        System.out.println("===========MANAGER==========");
        Manager mng = new Manager();
        mng.masukKerja();
        mng.beriNama("Bagus");
        mng.setJumlahAnggota(10);

        System.out.println("Nama : "+ mng.getNama());
        System.out.println("Jumlah Team : "+mng.getJumlahAnggota());
    }
}
